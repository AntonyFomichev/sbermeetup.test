import React from 'react';
import { Player } from 'containers/Player';

import './styles.scss';

export const App = () => {
  return (
    <div className="page">
      <Player />
    </div>
  );
};

type OffsetType =
  | number
  | { left?: number; top?: number; right?: number; bottom?: number };

/**
 *
 * @param element The element for which the cursor position will be checked.
 * @param event Mouse event.
 * @param offset Additional offset, if positive – the area will increase outward, if negative, then inward.
 */
export const isCursorInElement = (
  element: Element,
  event: MouseEvent,
  offset: OffsetType = 0
) => {
  const rect = element.getBoundingClientRect();
  const mouseX = event.clientX;
  const mouseY = event.clientY;

  if (typeof offset === 'number') {
    return (
      rect.left - offset < mouseX &&
      mouseX < rect.left + rect.width + offset &&
      rect.top - offset < mouseY &&
      mouseY < rect.top + rect.height + offset
    );
  }

  return (
    rect.left - (offset.left || 0) < mouseX &&
    mouseX < rect.left + rect.width + (offset.right || 0) &&
    rect.top - (offset.top || 0) < mouseY &&
    mouseY < rect.top + rect.height + (offset.bottom || 0)
  );
};

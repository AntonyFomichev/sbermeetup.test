import React, { forwardRef, ComponentPropsWithoutRef } from 'react';

import './styles.scss';

interface VideoProps extends ComponentPropsWithoutRef<'video'> {
  videoSrc: string;
}

export const Video = forwardRef<HTMLVideoElement, VideoProps>(
  ({ videoSrc, ...rest }, ref) => (
    <video className="player__video" ref={ref} controls={false} {...rest}>
      <source
        src={videoSrc}
        type={`video/${videoSrc.match(/\.[0-9a-z]+$/i)![0].slice(1)}`}
      />
    </video>
  )
);

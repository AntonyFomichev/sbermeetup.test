import React, { FC, HTMLAttributes } from 'react';

import { Button } from 'components/Button';

interface IPlaybackButton extends HTMLAttributes<HTMLButtonElement> {
  playbackState: 'play' | 'pause';
  togglePlay: () => void;
}

/**
 *
 * @param playbackState Accepts 'play' or 'pause' to choose icon of button.
 * @param togglePlay Function to toggle playback.
 *
 */
export const PlaybackButton: FC<IPlaybackButton> = ({
  playbackState,
  togglePlay,
  ...rest
}) => (
  <Button
    id="playback"
    onClick={togglePlay}
    iconCondition={playbackState === 'play'}
    icons={['pause', 'play']}
    iconSize={30}
    {...rest}
  />
);

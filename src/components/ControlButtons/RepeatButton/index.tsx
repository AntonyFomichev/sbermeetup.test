import React, { FC, HTMLAttributes } from 'react';

import { Button } from 'components/Button';

import './styles.scss';

interface IRepeatButton extends HTMLAttributes<HTMLButtonElement> {
  startVideo: () => void;
}

/**
 *
 * @param startVideo Function to video start over.
 *
 */
export const RepeatButton: FC<IRepeatButton> = ({ startVideo, ...rest }) => (
  <Button
    id="repeat"
    onClick={startVideo}
    className="button__repeat"
    iconCondition={true}
    icons={['repeat']}
    iconSize={100}
    {...rest}
  />
);

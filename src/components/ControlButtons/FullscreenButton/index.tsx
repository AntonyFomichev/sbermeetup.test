import React, { HTMLAttributes, FC } from 'react';

import { Button } from 'components/Button';

interface IFullscreenButton extends HTMLAttributes<HTMLButtonElement> {
  toggleFullscreen: () => void;
}

/**
 *
 * @param toggleFullscreen Function to toggle fullscreen of video element.
 *
 */
export const FullscreenButton: FC<IFullscreenButton> = ({
  toggleFullscreen,
  ...rest
}) => (
  <Button
    onClick={toggleFullscreen}
    iconCondition={true}
    icons={['fullscreen-not_active', 'fullscreen-active']}
    iconSize={20}
    {...rest}
  />
);

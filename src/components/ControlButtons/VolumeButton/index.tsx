import React, { FC, HTMLAttributes, ChangeEvent } from 'react';

import { Button } from 'components/Button';

import './styles.scss';

interface IVolumeButton extends HTMLAttributes<HTMLButtonElement> {
  muted: boolean;
  volumeState: number;
  toggleMute: () => void;
  changeVolume: (e: ChangeEvent<HTMLInputElement>) => void;
}

/**
 *
 * @param muted Change icon button depend on muted state.
 * @param volumeState Volume percent. Value from 0 to 1.
 * @param toggleMute Function to toggle muted state.
 * @param changeVolume Function that change volume on input volume range.
 *
 */
export const VolumeButton: FC<IVolumeButton> = ({
  muted,
  toggleMute,
  volumeState,
  changeVolume,
  ...rest
}) => {
  return (
    <div className="volume">
      <Button
        id="mute"
        onClick={toggleMute}
        className="button__volume"
        iconCondition={volumeState !== 0 && !muted}
        icons={['volume', 'mute']}
        iconSize={22}
        {...rest}
      >
        <input
          id="volume_range"
          type="range"
          min={0}
          max={1}
          step={0.05}
          width={20}
          value={volumeState}
          onChange={changeVolume}
        />
      </Button>
    </div>
  );
};

import React from 'react';
import { mapping } from './assets';

import './styles.scss';

export type IconType = keyof typeof mapping;

type IconProps = {
  glyph: IconType;
  size?: number;
  color?: string;
};

const iconStyles = ({ size, color, glyph }: IconProps) => ({
  height: `${size || 20}px`,
  width: `${size || 20}px`,
  WebkitMaskImage: `url(${mapping[glyph]})`,
  maskImage: `url(${mapping[glyph]})`,
  backgroundColor: `${color}`,
});

/**
 *
 * @param glyph Icon name from assets.
 * @param size Width and height of icon.
 * @param color Color of icon. Example: '#a2a2a2'
 *
 */
export const Icon = ({ size, color, glyph }: IconProps) => (
  <div className="icon" style={iconStyles({ size, color, glyph })} />
);

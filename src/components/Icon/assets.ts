import PlayImage from 'images/icons/play.svg';
import PauseImage from 'images/icons/pause.svg';
import SpeakerImage from 'images/icons/speaker.svg';
import MuteImage from 'images/icons/mute.svg';
import FullscreenActiveImage from 'images/icons/fullscreen-active.svg';
import FullscreenNotActiveImage from 'images/icons/fullscreen-unactive.svg';
import RepeatImage from 'images/icons/repeat.svg';

export const mapping: { [x: string]: string } = {
  play: PlayImage,
  pause: PauseImage,
  volume: SpeakerImage,
  mute: MuteImage,
  repeat: RepeatImage,
  'fullscreen-active': FullscreenActiveImage,
  'fullscreen-not_active': FullscreenNotActiveImage,
};

import React, {
  useState,
  useEffect,
  useCallback,
  useContext,
  MouseEvent,
} from 'react';

import { RepeatControlContext } from 'containers/Player';

import './styles.scss';

export const Progress = ({ video }) => {
  const [isMouseDown, setMouseDown] = useState(false);
  const [progress, setProgress] = useState('0%');

  const { setRepeatShow } = useContext(RepeatControlContext);

  /* setting flex-basis percent for progress filling depends on counting currentTime and duration */
  const handleProgress = useCallback(() => {
    const percent = (video.currentTime / video.duration) * 100;
    setProgress(`${percent}%`);
    setRepeatShow(percent > 95);
  }, [video, setRepeatShow]);

  useEffect(() => {
    video && video.addEventListener('timeupdate', handleProgress);

    return () => {
      video?.removeEventListener('timeupdate', handleProgress);
    };
  }, [video, handleProgress]);

  const startMouseDown = () => {
    setMouseDown(true);
  };

  const endMouseDown = () => {
    setMouseDown(false);
  };

  const scrub = (e: MouseEvent) => {
    const scrubTime =
      (e.nativeEvent.offsetX / video.clientWidth) * video.duration;
    if (!isNaN(scrubTime)) {
      video.currentTime = scrubTime;
      if (video.paused) video.play();
    }
  };

  return (
    <div
      id="progress"
      className="progress"
      onMouseUp={endMouseDown}
      onMouseDown={startMouseDown}
      onMouseMove={e => isMouseDown && scrub(e)}
      onMouseLeave={endMouseDown}
      onClick={scrub}
    >
      <div className="progress__filled" style={{ flexBasis: progress }} />
    </div>
  );
};

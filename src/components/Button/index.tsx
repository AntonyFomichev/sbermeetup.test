import React, { FC, HTMLAttributes } from 'react';

import { Icon, IconType } from 'components/Icon';

import './styles.scss';

interface IButton extends HTMLAttributes<HTMLButtonElement> {
  iconCondition?: boolean;
  icons: Array<IconType>;
  iconSize: number;
  children?: any;
}

/**
 *
 * @param iconCondition A condition that selects one of the two icons. Accepts boolean.
 * @param icons Array of two icons that appear depending on iconCondition. True is [0], false is [1]. If iconCondition is undefined set icon[0].
 * @param color Color of icon.
 * @param iconSize Size of icon in button. Accepts number.
 * @param children React element that comes inline after button.
 *
 */
export const Button: FC<IButton> = ({
  iconCondition,
  icons,
  iconSize,
  color,
  children,
  className,
  ...rest
}) => (
  <>
    <button className={`button ${className || ''}`} {...rest}>
      <Icon
        glyph={
          iconCondition || iconCondition === undefined ? icons[0] : icons[1]
        }
        size={iconSize}
        color={color}
      />
    </button>

    {children}
  </>
);

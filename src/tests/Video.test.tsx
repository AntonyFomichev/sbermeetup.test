import React from 'react';
import renderer from 'react-test-renderer';

import { Video } from 'components/Video';

describe('Video', () => {
  it('snapshot video', () => {
    const component = renderer.create(
      <Video videoSrc="https://s3-eu-west-1.amazonaws.com/onrewind-test-bucket/big_buck_bunny.mp4" />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

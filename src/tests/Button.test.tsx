import React from 'react';
import { shallow, render } from 'enzyme';
import renderer from 'react-test-renderer';

import { Button } from 'components/Button';
import { VolumeButton } from 'components/ControlButtons/VolumeButton';

describe('Button', () => {
  const onButtonClick = jest.fn();
  const _component = shallow(
    <Button onClick={onButtonClick} icons={['play', 'pause']} iconSize={30} />
  );

  it('snapshot common button', () => {
    const component = renderer.create(
      <Button onClick={onButtonClick} icons={['play', 'pause']} iconSize={30} />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('calls the passed in onclick function when button is clicked', () => {
    _component.find('button').simulate('click');
    expect(onButtonClick).toBeCalled();
  });

  it('check if icon appeared in button element', () => {
    const _container = render(
      <Button onClick={onButtonClick} icons={['play', 'pause']} iconSize={30} />
    );

    /* render button and try to get icon by class */
    expect(_container.find('.icon')).toHaveLength(1);
  });
});

describe('Volume Button', () => {
  const onButtonClick = jest.fn();
  let muted = false;
  let volume = 1;

  const _component = shallow(
    <VolumeButton
      muted={muted}
      toggleMute={() => {
        muted = !muted;
      }}
      volumeState={volume}
      changeVolume={e => {
        volume = parseFloat(e.target.value);
      }}
    />
  );

  it('snapshot volume button', () => {
    const component = renderer.create(
      <VolumeButton
        muted={muted}
        toggleMute={() => {
          muted = !muted;
        }}
        volumeState={volume}
        changeVolume={e => {
          volume = parseFloat(e.target.value);
        }}
      />
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should change volume on onchange', () => {
    const event = {
      target: {
        value: 0.5,
      },
    };

    expect(volume).toEqual(1);

    _component.find('input').simulate('change', event);
    expect(volume).toEqual(0.5);
  });

  it('should mute on button click', () => {
    // check if not muted
    expect(muted).toEqual(false);

    _component.find('.button__volume').simulate('click');
    expect(muted).toEqual(true);
  });
});

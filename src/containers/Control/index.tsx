import React, {
  useState,
  useEffect,
  useCallback,
  ChangeEvent,
  Dispatch,
} from 'react';

import { Progress } from 'components/Progress';
import { PlaybackButton } from 'components/ControlButtons/PlaybackButton';
import { VolumeButton } from 'components/ControlButtons/VolumeButton';
import { FullscreenButton } from 'components/ControlButtons/FullscreenButton';

import './styles.scss';

type ControlProps = {
  video: HTMLVideoElement | null;
  controlShown: boolean;
  volumeState: number;
  setVolumeState: Dispatch<number>;
};

export const Control = ({
  video,
  controlShown,
  volumeState,
  setVolumeState,
}: ControlProps) => {
  const [muted, setMute] = useState<boolean>(false);
  const [playbackState, setPlaybackState] = useState<'play' | 'pause'>('pause');

  /* TOGGLE PLAY MUTE FULLSCREEN */
  const toggleMute = useCallback(() => {
    setMute(muted => !muted);

    if (volumeState === 0) {
      setVolumeState(1);
      setMute(false);
    }
  }, [volumeState, setVolumeState]);

  const togglePlay = useCallback(() => {
    video && setPlaybackState(video.paused ? 'play' : 'pause');
  }, [video]);

  const toggleFullscreen = useCallback(() => {
    video && video.requestFullscreen();
  }, [video]);

  /* -------------------- */

  /* Handle SPACE and M */

  const detectMute = useCallback(
    e => {
      e.code === 'KeyM' && toggleMute();
    },
    [toggleMute]
  );

  const detectKeypress = useCallback(
    e => {
      e.code === 'Space' && togglePlay();
    },
    [togglePlay]
  );

  /* -------------------- */

  /* Play or Pause if playbackState changed */
  useEffect(() => {
    video && video[playbackState]();
  }, [video, playbackState]);

  /* -------------------- */

  useEffect(() => {
    if (video) {
      video.addEventListener('click', togglePlay);
      video.addEventListener('dblclick', toggleFullscreen);
      video.addEventListener('keydown', detectKeypress);
      video.addEventListener('keydown', detectMute);
    }

    return () => {
      video?.removeEventListener('click', togglePlay);
      video?.removeEventListener('dblclick', toggleFullscreen);
      video?.removeEventListener('keydown', detectKeypress);
      video?.removeEventListener('keydown', detectMute);
    };
  }, [video, togglePlay, toggleFullscreen, detectKeypress, detectMute]);

  useEffect(() => {
    if (video) video.volume = volumeState;
  }, [video, volumeState]);

  useEffect(() => {
    if (video) video.muted = muted;
  }, [video, muted]);

  const changeVolume = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setVolumeState(parseFloat(value));
  };

  return (
    <div
      id="control"
      className={`control control__gradient ${
        controlShown ? 'control__fade-in' : 'control__fade-out'
      }`}
    >
      <Progress video={video} />

      <div className="control__side_left">
        <PlaybackButton playbackState={playbackState} togglePlay={togglePlay} />

        <VolumeButton
          muted={muted}
          volumeState={volumeState}
          toggleMute={toggleMute}
          changeVolume={changeVolume}
        />
      </div>

      <FullscreenButton toggleFullscreen={toggleFullscreen} />
    </div>
  );
};

import React, { useEffect, useRef, useState, createContext } from 'react';

import { Control } from 'containers/Control';

import { Video } from 'components/Video';
import { RepeatButton } from 'components/ControlButtons/RepeatButton';

import './styles.scss';

/* to pass throw Control to repeat button (avoid props drilling) */
export const RepeatControlContext = createContext({
  setRepeatShow: (() => ({})) as (flag: boolean) => void,
});

export const Player = () => {
  const [video, setVideo] = useState<HTMLVideoElement | null>(null);
  const [volumeState, setVolumeState] = useState<number>(1);
  const [controlShown, setControlShow] = useState<boolean>(false);
  const [isRepeatShown, setRepeatShow] = useState<boolean>(false);
  const [timeoutLeave, setTimeoutLeave] = useState<
    ReturnType<typeof setTimeout>
  >();

  const videoRef = useRef<HTMLVideoElement | null>(null);

  useEffect(() => {
    if (videoRef) {
      setVideo(videoRef.current);
    }
  }, []);

  const onMouseOverVideo = () => {
    setControlShow(true);

    timeoutLeave && clearTimeout(timeoutLeave);
  };

  const onMouseLeave = () => {
    const timeout = setTimeout(() => {
      setControlShow(false);
    }, 2000);

    setTimeoutLeave(timeout);
  };

  const startOver = () => {
    if (video) {
      video.currentTime = 0;
      video.play();
    }
  };

  return (
    <div
      className="player"
      onMouseMove={onMouseOverVideo}
      onMouseLeave={onMouseLeave}
    >
      <Video
        id="player"
        ref={videoRef}
        videoSrc="https://s3-eu-west-1.amazonaws.com/onrewind-test-bucket/big_buck_bunny.mp4"
        preload="metadata"
      />

      {isRepeatShown && <RepeatButton startVideo={startOver} />}

      <RepeatControlContext.Provider value={{ setRepeatShow }}>
        <Control
          video={video}
          controlShown={controlShown}
          volumeState={volumeState}
          setVolumeState={setVolumeState}
        />
      </RepeatControlContext.Provider>
    </div>
  );
};

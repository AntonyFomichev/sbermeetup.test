import React, { useRef, useEffect, useState } from 'react';

import { Video } from 'components/Video';
import { Progress } from 'components/Progress';

const ProgressInfo = {
  title: 'Progress',
  component: Progress,
  argTypes: {},
  param: {},
};

export const Primary = ({ onClick, ...args }) => {
  const videoRef = useRef(null);
  const [video, setVideo] = useState<HTMLVideoElement | null>(null);

  useEffect(() => {
    if (videoRef) {
      setVideo(videoRef.current);
    }
  }, []);

  return (
    <div
      style={{ position: 'relative', maxWidth: '640px', overflow: 'hidden' }}
    >
      <Video
        ref={videoRef}
        videoSrc="https://s3-eu-west-1.amazonaws.com/onrewind-test-bucket/big_buck_bunny.mp4"
      />
      <Progress video={video} {...args} />
    </div>
  );
};
Primary.args = {};

export default ProgressInfo;

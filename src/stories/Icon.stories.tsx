import React from 'react';

import { Icon } from 'components/Icon';

const IconInfo = {
  title: 'Icon',
  component: Icon,
  argTypes: {
    glyph: { control: { type: 'text' } },
    size: { control: { type: 'number' } },
    color: { control: { type: 'color' } },
  },
  param: {},
};

export const Primary = ({ onClick, ...args }) => (
  <Icon glyph="play" {...args} />
);
Primary.args = { color: '#AA6262', size: 50 };

export default IconInfo;

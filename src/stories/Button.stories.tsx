import React from 'react';
import { Button } from 'components/Button';

import { FullscreenButton } from 'components/ControlButtons/FullscreenButton';
import { PlaybackButton } from 'components/ControlButtons/PlaybackButton';
import { RepeatButton } from 'components/ControlButtons/RepeatButton';
import { VolumeButton } from 'components/ControlButtons/VolumeButton';

const ButtonInfo = {
  title: 'Button',
  component: Button,
  argTypes: {
    onClick: { action: 'clicked' },
    color: { control: { type: 'color' } },
  },
  param: {},
};

export const Primary = ({ onClick, ...args }) => (
  <Button
    onClick={onClick}
    iconCondition={false}
    icons={['pause', 'play']}
    iconSize={20}
    {...args}
  />
);
Primary.args = { color: '#AA6262', iconSize: 50 };

export const Fullscreen = ({ onClick }) => (
  <div style={{ background: '#222' }}>
    <FullscreenButton toggleFullscreen={onClick} />
  </div>
);
Fullscreen.args = {};

export const Playback = ({ onClick }) => (
  <div style={{ background: '#222' }}>
    <PlaybackButton togglePlay={onClick} playbackState={'play'} />
    <PlaybackButton togglePlay={onClick} playbackState={'pause'} />
  </div>
);
Playback.args = {};

export const Repeat = ({ onClick }) => (
  <div style={{ background: '#646464', width: '100vw', height: '100vh' }}>
    <RepeatButton startVideo={onClick} />
  </div>
);
Repeat.args = {};

export const Volume = ({ onClick }) => (
  <div style={{ background: '#222', padding: '0 4rem' }}>
    <VolumeButton
      volumeState={1}
      muted={false}
      toggleMute={onClick}
      changeVolume={onClick}
    />
    <VolumeButton
      volumeState={1}
      muted={true}
      toggleMute={onClick}
      changeVolume={onClick}
    />
  </div>
);
Volume.args = {};

export default ButtonInfo;

import React from 'react';

import { Player } from 'containers/Player';

const PlayerInfo = {
  title: 'Player',
  component: Player,
  argTypes: {},
};

export const Primary = ({ ...args }) => {
  return <Player {...args} />;
};
Primary.args = {};

export default PlayerInfo;

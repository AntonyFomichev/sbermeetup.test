import React from 'react';

import { Video } from 'components/Video';

const VideoInfo = {
  title: 'Video',
  component: Video,
  argTypes: {
    videoSrc: { control: { type: 'text' } },
  },
};

export const Primary = ({ onClick, ...args }) => {
  return (
    <div
      style={{ position: 'relative', maxWidth: '640px', overflow: 'hidden' }}
    >
      <Video videoSrc={args.videoSrc} {...args} />
    </div>
  );
};
Primary.args = {
  videoSrc:
    'https://s3-eu-west-1.amazonaws.com/onrewind-test-bucket/big_buck_bunny.mp4',
};

export default VideoInfo;

module.exports = {
  stories: ['../src/stories/*.stories.tsx'],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-controls',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/preset-create-react-app',
  ],
  webpackFinal: async (config) => {
    const scopePluginIndex = config.resolve.plugins.findIndex(
      ({ constructor }) =>
        constructor && constructor.name === 'ModuleScopePlugin'
    );

    config.resolve.plugins.splice(scopePluginIndex, 1);
    return config;
  },
};

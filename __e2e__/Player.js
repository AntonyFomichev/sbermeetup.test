import { Selector, ClientFunction } from 'testcafe';

fixture`Test Player`.page`http://localhost:3000/`;

test('Video E2E Test', async t => {
  const video = Selector('#player');
  const control = Selector('#control');
  const muteButton = Selector('#mute');
  const playButton = Selector('#playback');
  const range = Selector('#volume_range');
  const repeatButton = Selector('#repeat');
  const progress = Selector('#progress');

  const videoPaused = ClientFunction(() => video().paused, {
    dependencies: { video },
  });

  const videoMuted = ClientFunction(() => video().muted, {
    dependencies: { video },
  });

  const volumeValue = ClientFunction(() => range().value, {
    dependencies: { range },
  });

  const currentTime = ClientFunction(() => video().currentTime, {
    dependencies: { video },
  });

  const setTime = ClientFunction(time => (video().currentTime = time), {
    dependencies: { video },
  });

  /* control is visible */
  await t.hover(video).expect(control.visible).ok();

  /* should video playing */
  await t.hover(video).click(playButton).expect(videoPaused()).eql(false);

  /* should video muted */
  await t.click(muteButton).expect(videoMuted()).eql(true);

  /* should video volume set to 30%' */
  await t.typeText(range, '0.3').expect(volumeValue()).eql('0.3');

  /* should video volume set to 0%' */
  await t.typeText(range, '0').expect(volumeValue()).eql('0');

  /* if volume 0 and click on muted: volume should be 100% */
  await t.click(muteButton).expect(volumeValue()).eql('1');

  /* setting current time on 58 seconds */
  await setTime(58);

  /* should video start over */
  await t.click(repeatButton).expect(currentTime()).gt(-4);

  /* should video current time set to half of video duration */
  await t.click(progress).expect(currentTime()).lt(31);
});
